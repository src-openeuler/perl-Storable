%define mod_name Storable

Name:    perl-%{mod_name}
Epoch:   1
Version: 3.32
Release: 2
Summary: Persistence for Perl data structures
License: GPL-1.0-or-later OR Artistic-1.0-Perl
URL:     https://metacpan.org/release/Storable
Source0: https://cpan.metacpan.org/authors/id/N/NW/NWCLARK/%{mod_name}-3.25.tar.gz
Patch0:  Storable-3.25-Upgrade-to-3.32.patch
Patch9000: eliminate-limitpm-difference.patch

BuildRequires: bash, gcc, make, perl-devel, perl-generators, perl-interpreter
BuildRequires: perl(Config), perl(Cwd), perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires: perl(File::Copy), perl(File::Spec) >= 0.8
BuildRequires: perl(strict), perl(warnings), perl(Carp), perl(Exporter)
BuildRequires: perl(Fcntl), perl(IO::File), perl(XSLoader)
BuildRequires: perl(base), perl(bytes), perl(File::Temp), perl(integer)
BuildRequires: perl(overload), perl(utf8), perl(Test::More), perl(threads)
BuildRequires: perl(Safe), perl(Scalar::Util), perl(Tie::Array), perl(B::Deparse) >= 0.61
BuildRequires: perl(Digest::MD5), perl(Hash::Util), perl(Tie::Hash)
Requires: perl(Carp), perl(Config), perl(Fcntl), perl(IO::File)

# Filter modules bundled for tests
%global __provides_exclude_from %{?__provides_exclude_from:%__provides_exclude_from|}^%{_libexecdir}
%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\(HAS_OVERLOAD\\)
%global __requires_exclude %{__requires_exclude}|^perl\\(testlib.pl\\)

%description
The Storable extension brings persistence to your data.
You may recursively store to disk any data structure, no matter how
complex and circular it is, provided it contains only SCALAR, ARRAY,
HASH (possibly tied) and references (possibly blessed) to those items.

%package_help

%package tests
Summary:        Tests for %{name}
Requires:       %{name} = %{?epoch:%{epoch}:}%{version}-%{release}
Requires:       perl-Test-Harness
Requires:       perl(B::Deparse) >= 0.61
Requires:       perl(Digest::MD5)

%description tests
Tests from %{name}. Execute them
with "%{_libexecdir}/%{name}/test".

%prep
%autosetup -n %{mod_name}-3.25 -p1

# Help generators to recognize Perl scripts
for F in t/*.t t/*.pl; do
    perl -i -MConfig -ple 'print $Config{startperl} if $. == 1 && !s{\A#!.*perl\b}{$Config{startperl}}' "$F"
    chmod +x "$F"
done

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 NO_PERLLOCAL=1 OPTIMIZE="%{optflags}"
%make_build

%install
make pure_install DESTDIR=%{buildroot}
find $RPM_BUILD_ROOT -type f -name '*.bs' -size 0 | xargs rm -f
find $RPM_BUILD_ROOT -type f -name '*.3pm' -size 0 | xargs rm -f
%{_fixperms} %{buildroot}/*

# Install tests
mkdir -p %{buildroot}/%{_libexecdir}/%{name}
cp -a t %{buildroot}/%{_libexecdir}/%{name}
cat > %{buildroot}/%{_libexecdir}/%{name}/test << 'EOF'
#!/bin/bash
set -e
# Some tests write into temporary files/directories. The easiest solution
# is to copy the tests into a writable directory and execute them from there.
DIR=$(mktemp -d)
pushd "$DIR"
cp -a %{_libexecdir}/%{name}/* ./
prove -I . -j "$(getconf _NPROCESSORS_ONLN)"
popd
rm -rf "$DIR"
EOF
chmod +x %{buildroot}/%{_libexecdir}/%{name}/test

%check
unset PERL_TEST_MEMORY PERL_RUN_SLOW_TESTS
make test

%files
%doc ChangeLog README
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/Storable*

%files help
%{_mandir}/man*/*

%files tests
%{_libexecdir}/%{name}

%changelog
* Tue Jan 07 2025 Funda Wang <fundawang@yeah.net> - 1:3.32-2
- cleanup spec

* Wed Sep  4 2024 dillon chen <dillon.chen@gmail.com> - 1:3.32-1
- upgrade version to 3.32(Storable-3.25-Upgrade-to-3.32.patch from Redhat)

* Sat Nov 26 2022 zhoushuiqing <zhoushuiqing2@huawei.com> - 3.25-3
- Update the Source0 URL.

* Tue Jun 28 2022 Chenyx <chenyixiong3@huawei.com> - 3.25-2
- License compliance rectification

* Thu Nov 18 2021 liudabo <liudabo1@huawei.com> - 3.25-1
- upgrade version to 3.25

* Tue Feb 25 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.15-2
- Modify subpakcage help

* Mon Sep 16 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.15-1
- Package init
